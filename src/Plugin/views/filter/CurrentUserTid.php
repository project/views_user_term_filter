<?php

namespace Drupal\views_user_term_filter\Plugin\views\filter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter by term id drawn from the current user.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("current_user_tid")
 */
class CurrentUserTid extends FilterPluginBase {

  /**
   * Constructs a TaxonomyIndexTid object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->valueOptions = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    if (!empty($this->definition['vocabulary'])) {
      $this->options['vid'] = $this->definition['vocabulary'];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function query() {
    // Retrieve the configured content field.
    $content_field = Xss::filterAdmin($this->options['content_field']);

    $user_proxy = \Drupal::currentUser();
    if (!$user_proxy->isAuthenticated() or empty($content_field)) {
      // No need to process for anonymous users or if no field specified.
      return;
    }
    // Get the current user's value for the specified field.
    $user_field = Xss::filterAdmin($this->options['user_field']);
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($user_proxy->id());
    $user_val = $user->get($user_field)->target_id;

    if (empty($user_val) && $this->options['empty']) {
      // No need to process if no user value set, and configured to ignore this.
      return;
    }

    // Assemble the table and field names needed for the query.
    $table_field = $content_field . '_target_id';
    $views_data = Views::viewsData();
    $base_table = $this->view->storage->get('base_table');
    $base_table_data = $views_data->get($base_table);
    $entity_type = $base_table_data['table']['entity type'];
    $table = $entity_type . '__' . $content_field;

    $this->query
      ->ensureTable($table);
    $this->query
      ->addWhere($this->options['group'], "$table.$table_field", $user_val, '=');
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['user_field']['default'] = '';
    $options['content_field']['default'] = '';
    $options['empty']['default'] = '1';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $field_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config');
    $user_fields_raw = $field_storage->loadByProperties(
      [
        'settings' => [
          'target_type' => 'taxonomy_term',
        ],
        'entity_type' => 'user',
        'type' => 'entity_reference',
        'deleted' => FALSE,
        'status' => 1,
      ]
    );
    $field_config = \Drupal::entityTypeManager()->getStorage('field_config');
    // Load user field labels.
    $user_fields_config = $field_config->loadByProperties(
      [
        'entity_type' => 'user',
        'field_type' => 'entity_reference',
        'status' => 1,
      ]
    );
    $field_labels = [];
    foreach ($user_fields_config as $user_field) {
      $field_labels[$user_field->getName()] = $user_field->getLabel();
    }
    foreach ($user_fields_raw as $user_field_raw) {
      $user_fields[$user_field_raw->getName()] = $field_labels[$user_field_raw->getName()] ?? $user_field_raw->getName();
    }
    // Use the entity_type defined for the view.
    $entity_type = $this->view->getBaseEntityType()->id();

    $node_fields_raw = $field_storage->loadByProperties(
      [
        'settings' => [
          'target_type' => 'taxonomy_term',
        ],
        'entity_type' => $entity_type,
        'type' => 'entity_reference',
        'deleted' => FALSE,
        'status' => 1,
      ]
    );

    $node_fields_config = $field_config->loadByProperties(
      [
        'entity_type' => $entity_type,
        'field_type' => 'entity_reference',
        'status' => 1,
      ]
    );
    $field_labels = [];
    foreach ($node_fields_config as $node_field) {
      $field_labels[$node_field->getName()] = $node_field->getLabel();
    }
    foreach ($node_fields_raw as $node_field_raw) {
      $node_fields[$node_field_raw->getName()] = $field_labels[$node_field_raw->getName()] ?? $node_field_raw->getName();
    }

    $form['user_field'] = [
      '#type' => 'select',
      '#options' => $user_fields,
      '#title' => $this->t('User field'),
      '#default_value' => $this->options['user_field'],
      '#description' => $this->t('The user profile field from which to retrieve the term.'),
      '#required' => TRUE,
    ];

    // @todo Validate that the specified user_field and content_field have at
    // at least one vocabulary in common. If not no results are possible.
    $form['content_field'] = [
      '#type' => 'select',
      '#options' => $node_fields,
      '#title' => $this->t('Content field'),
      '#default_value' => $this->options['content_field'],
      '#description' => $this->t('The content field in which boost a match.'),
      '#required' => TRUE,
    ];

    $form['empty'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->options['empty'],
      '#title' => $this->t('Show all results if no user value'),
      '#description' => $this->t('If unchecked, no results will be shown if the user field does not contain a value.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $user_field = Xss::filterAdmin($this->options['user_field']);
    return Cache::mergeContexts(parent::getCacheContexts(), ['user.ref_field:' . $user_field]);
  }

}
